const NsApiWrapper = require('netsuite-rest');
let NsApi = new NsApiWrapper({
	consumer_key: '1ef5b24928e18e0a36b848d8597bb8867bbd951fdb4eb46380b2971363200506',
	consumer_secret_key: 'a319aeba9e493afd74b620fca7500d8cd1aae67b52eb79fd6f9560e24b04cbb5',
	token: '7a6e06ea30d5afc5d4e00d5583ea74e12befe93473081c1ee9ad9f2a7ae68b60',
	token_secret: '60020d0243fdc1ac60265424a49277ec6a4796ba96d6e3c30f97b7e44ab03ea7',
	realm: '5454552'
});

export function fetchWarehouses() {
    NsApi.request({
        path: 'https://5454552.suitetalk.api.netsuite.com/services/rest/query/v1/suiteql',
        method: "POST",
        body: `{
            "q": 
            "SELECT BUILTIN_RESULT.TYPE_STRING(BUILTIN.DF(LocationSubsidiaryMap_SUB.country)) AS country, BUILTIN_RESULT.TYPE_INTEGER(LOCATION.ID) AS ID, BUILTIN_RESULT.TYPE_STRING(LOCATION.name) AS name, BUILTIN_RESULT.TYPE_STRING(LocationSubsidiaryMap_SUB.state) AS state, BUILTIN_RESULT.TYPE_STRING(BUILTIN.DF(LOCATION.subsidiary)) AS subsidiary FROM LOCATION, (SELECT LocationSubsidiaryMap.LOCATION AS LOCATION, Subsidiary.country AS country, Subsidiary.state AS state FROM LocationSubsidiaryMap, Subsidiary WHERE LocationSubsidiaryMap.subsidiary = Subsidiary.ID) LocationSubsidiaryMap_SUB WHERE LOCATION.ID = LocationSubsidiaryMap_SUB.LOCATION(+) AND ((NVL(LOCATION.isinactive, 'F') = 'F' AND LOCATION.makeinventoryavailable = 'T'))"
        }`
    })
    .then(response => console.log(response))
    .catch((err) => console.log(err));
}


// const requestOptions = {
// 	method: "POST",
// 	headers: {
// 		'Content-Type': 'application/json'
// 	},
// 	body: `{
// 		q: "SELECT BUILTIN_RESULT.TYPE_STRING(BUILTIN.DF(LocationSubsidiaryMap_SUB.country)) AS country, BUILTIN_RESULT.TYPE_INTEGER(LOCATION.ID) AS ID, BUILTIN_RESULT.TYPE_STRING(LOCATION.name) AS name, BUILTIN_RESULT.TYPE_STRING(LocationSubsidiaryMap_SUB.state) AS state, BUILTIN_RESULT.TYPE_STRING(BUILTIN.DF(LOCATION.subsidiary)) AS subsidiary FROM LOCATION, (SELECT LocationSubsidiaryMap.LOCATION AS LOCATION, Subsidiary.country AS country, Subsidiary.state AS state FROM LocationSubsidiaryMap, Subsidiary WHERE LocationSubsidiaryMap.subsidiary = Subsidiary.ID) LocationSubsidiaryMap_SUB WHERE LOCATION.ID = LocationSubsidiaryMap_SUB.LOCATION(+) AND ((NVL(LOCATION.isinactive, 'F') = 'F' AND LOCATION.makeinventoryavailable = 'T'))",
//     }`,
// };

// export function fetchWarehouses() {
// 	fetch("https://5454552.suitetalk.api.netsuite.com/services/rest/query/v1/suiteql", requestOptions)
// 		.then(async (response) => {
// 			const data = await response.json();
// 			console.log('response', data);
// 		})
// 		.catch((error) => {
// 			console.error("There was an error!", error);
// 		});
// }