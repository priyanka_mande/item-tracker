import * as React from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Paper } from '@mui/material';

function createData(name, oem, qty) {
  return { name, oem, qty };
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0),
  createData('Ice cream sandwich', 237, 9.0),
  createData('Eclair', 262, 16.0),
  createData('Cupcake', 305, 3.7),
  createData('Gingerbread', 356, 16.0),
];

export default function DisplayResult(props) {
  return (
    <>
    <TextField fullWidth variant="standard" label="Search" id="search-result" sx={{ mt: 2, mb: 3 }} />
    <TableContainer component={Paper}>
      <Table aria-label="result table">
        <TableHead>
          <TableRow>
            <TableCell>{props && props.type === 'bin' ? 'Item Code' : 'Bin Number' }</TableCell>
            <TableCell>OEM</TableCell>
            <TableCell align="right">Available Qty</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">{row.name}</TableCell>
              <TableCell>{row.oem}</TableCell>
              <TableCell align="right">{row.qty}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </>
  );
}


/*
TODO:
Add search functionality
ap table data properly
*/