import * as React from 'react';
import { FormLabel, RadioGroup, FormControlLabel, Radio, Grid, TextField, Autocomplete, Tooltip, IconButton } from '@mui/material';
import AutorenewIcon from '@mui/icons-material/Autorenew';
import BinNumber from './BinNumber';
import ItemCode from './ItemCode';
import { fetchWarehouses } from '../api';

export default function Warehouse() {
  const [warehouse, setWarehouse] = React.useState('');
  const [searchValue, setSearchValue] = React.useState('');

  const handleSearchValueChange = (event) => {
    setSearchValue(event.target.value);
  };

  const refreshWarehouseList = () => {
    
  }

  React.useEffect(() => {
    fetchWarehouses();
    //localStorage.setItem('Locations', )
  });

  const warehouseList = [
    {'label': 'Pune'},
    {'label': 'Fremont'}
  ];

  return (
    <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }} sx={{ mt:2 }}>
        <Grid container xs={12} direction="row" justifyContent="center" alignItems="center">
            <Grid item xs={1}></Grid>
            <Grid item xs={9}>
                <Autocomplete
                    sx={{ mr: 2 }}
                    disablePortal
                    id="select-warehouse"
                    options={warehouseList}
                    value={warehouse.label}
                    onChange={(selectedWarehouse) => {
                        setWarehouse(selectedWarehouse);
                    }}
                    renderInput={(params) => <TextField {...params} label="Select Warehouse" />}
                />
            </Grid>
            <Grid item xs={2}>
                <Tooltip title="Fetch from Netsuite">
                    <IconButton onClick={refreshWarehouseList}>
                        <AutorenewIcon color="primary"  />
                    </IconButton>
                </Tooltip>
            </Grid>
        </Grid>
        {warehouse && <Grid container xs={12} direction="row" justifyContent="center" alignItems="center" sx={{ mt: 3 }}>
            <FormLabel component="legend" sx={{ mr: 3 }}>Search via: </FormLabel>
            <RadioGroup 
                row 
                aria-label="searchBy" 
                name="searchBy" 
                value={searchValue}
                onChange={handleSearchValueChange}
            >
                <FormControlLabel value="bin" control={<Radio />} label="Bin Number" />
                <FormControlLabel value="item" control={<Radio />} label="Item Code" />      
            </RadioGroup>
        </Grid>}
        <Grid container xs={12} direction="row" justifyContent="center" alignItems="center" sx={{ mt: 3 }}>
            { searchValue === 'bin' ? <BinNumber /> : searchValue === "item" ? <ItemCode /> : null }
        </Grid>
    </Grid>
  );
}

/**
TODO: 
Refresh button disable till everything is complete
 */