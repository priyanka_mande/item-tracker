import * as React from 'react';
import { TextField, Autocomplete, Grid, IconButton, Tooltip } from '@mui/material';
import AutorenewIcon from '@mui/icons-material/Autorenew';
import DisplayResult from './DisplayResult';

export default function BinNumber() {
  const [binNumber, setBinNumber] = React.useState('');

  const refreshBinList = () => {

  }

  const warehouseList = [
    {'label': 'Pune'},
    {'label': 'Fremont'}
  ];

  return (
        <>
            <Grid container xs={6} direction="row" justifyContent="center" alignItems="center">
                <Grid item xs={10}>
                    <Autocomplete
                        sx={{ mr: 2 }}
                        disablePortal
                        id="select-bin-number"
                        options={warehouseList}
                        value={binNumber.label}
                        onChange={(selectedBinNumber) => {
                            setBinNumber(selectedBinNumber);
                        }}
                        renderInput={(params) => <TextField {...params} label="Select Bin" />}
                    />
                </Grid>
                <Grid item xs={2}>
                    <Tooltip title="Fetch from Netsuite">
                        <IconButton onClick={refreshBinList}>
                            <AutorenewIcon color="primary"  />
                        </IconButton>
                    </Tooltip>
                </Grid>
            </Grid>
            <Grid container xs={11} direction="row" justifyContent="center" alignItems="center" sx={{ mt: 3 }}>
                {binNumber && <DisplayResult type="bin" data={binNumber} />}
            </Grid>
        </>
    );
}

/*
TODO:
Add api call to get bin list
Send actual result in DisplayResult component
add refresh button to fetch bin numbers from netsuite
*/