import * as React from 'react';
import { Grid, FormHelperText , TextField, Autocomplete, IconButton, Tooltip } from '@mui/material';
import AutorenewIcon from '@mui/icons-material/Autorenew';
import DisplayResult from './DisplayResult';

export default function BinNumber() {
  const [oemName, setOemName] = React.useState('');
  const [itemCode, setItemCode] = React.useState('');

  const refreshOEMList = () => {

  }

  const warehouseList = [
    {'label': 'Pune'},
    {'label': 'Fremont'}
  ];

  return (
    <>
        <Grid item xs={6}>
            <Grid container xs={12} direction="row" justifyContent="center" alignItems="center" sx={{ mt: 3 }}>
                <Grid item xs={11}>
                    <Autocomplete
                        sx={{ mr: 2 }}
                        disablePortal
                        id="select-oem"
                        options={warehouseList}
                        value={oemName.label}
                        onChange={(selectedOEM) => {
                            setOemName(selectedOEM);
                        }}
                        renderInput={(params) => <TextField {...params} label="Select OEM Name" />}
                    />
               </Grid>
               <Grid item xs={1}>
                    <Tooltip title="Fetch from Netsuite">
                        <IconButton onClick={refreshOEMList}>
                            <AutorenewIcon color="primary"  />
                        </IconButton>
                    </Tooltip>
                </Grid>
            </Grid>
            <FormHelperText>Optional</FormHelperText>
            <Autocomplete
                sx={{ mt: 2 }}
                disablePortal
                id="select-oem"
                options={warehouseList}
                value={itemCode.label}
                onChange={(selectedItem) => {
                    setItemCode(selectedItem);
                }}
                renderInput={(params) => <TextField {...params} label="Select Item Code" />}
            />
        </Grid>
        <Grid container xs={11} direction="row" justifyContent="center" alignItems="center" sx={{ mt: 3 }}>
            {itemCode && <DisplayResult type="item" data={itemCode} />}
        </Grid>
    </>
  );
}

/*
TODO:
Add api call to get oem list
Add api call to get item list
Send actual result in DisplayResult component
*/