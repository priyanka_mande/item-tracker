import React from "react";
import ReactDOM from 'react-dom';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { CssBaseline, Box, AppBar, Toolbar, Link, Typography, IconButton, Container, Paper } from '@mui/material';
import Warehouse from './components/Warehouse';
import './style/style.css';

const ItemTracker = React.memo(() => {
  function Copyright(props) {
    return (
      <Typography variant="body2" color="text.secondary" align="center" {...props}>
        {'Copyright © '}
        <Link color="inherit" href="#">
          Rahi Systems
        </Link>{' '}
        {new Date().getFullYear()}
      </Typography>
    );
  }
  
  const mdTheme = createTheme();

  return (
    <ThemeProvider theme={mdTheme}>
      <Box sx={{ display: 'flex' }}>
        <CssBaseline />
        <AppBar position="fixed">
          <Toolbar>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              sx={{ flexGrow: 1 }}
            >
              Item Tracker
            </Typography>
            <IconButton color="inherit">
              
            </IconButton>
          </Toolbar>
        </AppBar>
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === 'light'
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: '100vh',
            overflow: 'auto',
          }}
        >
          <Toolbar />
          <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
            <Paper sx={{ p: 4 }}>
              <Warehouse /> 
            </Paper>
            <Copyright sx={{ pt: 4 }} />
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  );
});

ReactDOM.render(<ItemTracker />, document.getElementById("root"));


/*
TODO:
Add express js to get & save warehouse & bin data
API wait mechanism overall
*/